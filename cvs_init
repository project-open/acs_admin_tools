#!/usr/bin/perl

use strict;
# Getopt, to accept command line switches
use Getopt::Std;

#<title>
# cvs_init
#</title>

#<abstract>
# creates a cvs repository for the server 
# imports the existing development server into the new repository
# blows away the existing code 
# does a checkin and checkout 
# and finally appends a line to the crontab to do nightly checkin and checkout
#
#</abstract>

#<details>
# 1. Do the usual sanity check for new user
# 2. Create a cvs repository cvs -d $cvsroot init
# 3. Delete the currently existing code (except possible the parameters directory)
# 4. import code from the production repository into the new server
# 5. check the new code in (to start the repository) and check it out (to work on)
# 6. create cron job to check code in every night (to record changes) and check it out again (for working on it).
#</details>


##########################################
#               Script                   #
##########################################

my $debug;
my $logfile;
my $command;
my $result;
my $errors;
my $err_msg;

my $acs_source_dir;

my $new_user_tablespace;	# tablespace to create
my $new_user;			# ora user to create
my $new_user_pwd;		# ora user passwd
my $new_user_type;		# student or developer

my $oracle_base;		# Oracle env variable
my $oracle_home;		#	
my $oracle_bin;			#

my $sql_query;			# generic query container
my $sql_result;			# generic result container

# vars for the getdate function
#use vars qw/ $sec $min $hour $mday $mon $year $wday $yday $isdst $theyear $themonth $mday $date $time /;

##########################################
#             Configuration              #
##########################################

# Do you want debug text printed?
$debug=1;

# Where is the logfile?
$logfile = "/tmp/acs_setup-error.log";

# Where is the ACS source?
# $acs_source_dir = "/usr/local/aolserver/current_acs";

##########################################
#               Script                   #
##########################################

# Getopts: Which options to look for, one letter for each option
# letters followed by a ':' take arguments, others are boolean,
# returning 1 when the flag is set.
getopts("s:p:a:");
# So "strict" won't complain
use vars qw/ $opt_s $opt_p $opt_a /;

&handle_args;


#must be nsadmin to execute
#&must_be_user("nsadmin");

&check_out



##########################################
#             Subroutines                #
##########################################

sub handle_args {
  # Getopts: Which options to look for, one letter for each option
  # letters followed by a ':' take arguments, others are boolean,
  # returning 1 when the flag is set.
  # For now, use the short list.
  getopts("s:a:h");
  #getopts("p:s:t:i:a:u:h");
  # So "strict" won't complain
  use vars qw/ $opt_p $opt_s $opt_t $opt_i $opt_a $opt_u $opt_h /;
  
  
  #if (!($ARGV[0])) {
  # Later you might want to write a simple question and answer interface to 
  # configure the args if none are entered on the command line.
  #if ($opt_h)) {
  if ( ($opt_h) || (!($ARGV[0])) ) {
    print "Usage: acs_load_tables [options] new_user\n";
    print "To use default configuration (recommended) do not use any options.\n";
    print "options: acs_load_tables [-h(elp)] [-p password] [-t tablespace_name] [-a acs_source_directory] new_user\n";
    print "Default tablespace_name and password are same as new_user\n";
    exit;
  }
  else {
    $new_server = $new_user = $ARGV[0];
  }
    
  # Password and username are the same unless set on the command line.
  if ($opt_p) {
    $new_user_pwd = $opt_p;
  }
  else {
    $new_user_pwd = $new_user;
  }
  
  
  # Tablespace and user have the same name unless set on command line
  if ($opt_t) {
    $new_user_tablespace = $opt_t;
  }
  else {
    $new_user_tablespace = $new_user;
  }
  
  
  # User type ($opt_u) defaults to Student unless -u developer typed on command line
  if ($opt_u =~ /dev/) {
    $new_user_type = "developer";
  }
  else {
    $new_user_type = "student";
  }
  
  
  # Use default ACS source directory unless set on commandline
  if ($opt_a != 0) {
    $acs_source_dir = $opt_a;
  }

}



sub must_be_user {

  # Given a "required" user's name, find that user's UID and make sure
  # that the current user is the required user. Else, exit;
  my $user = shift(@_);

  print "required user: $user\n" if $debug;
  #read the passwd file
  open(PASSWD, "/etc/passwd") or die "Couldn't open /etc/passwd for reading on line: ", __LI
NE__, "\n";

  # find the line with our user
  my $line;
  my $user_id;
  LOOP: foreach $line (<PASSWD>) {
      $user_id = (split(":",$line))[2];

    if ($line =~ /^$user:/) {
      $user_id = (split(":",$line))[2];
      print "We think you're user id is: $< and we want you to be $user_id\n" if $debug;
      if ($< != $user_id) {
        print "Only $user may execute this script.\n";
        exit;
      }
      else {
        last LOOP;
      }
    }
  }
}


sub get_date_time {
# vars for the getdate function
use vars qw/ $sec $min $hour $mday $mon $year $wday $yday $isdst $theyear $themonth $mday $date $time /;

  # Today's date in the format God intended: YYYY-MM-DD
  ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
  $theyear=($year + 1900);
  $themonth=($mon + 1);

  # We need the month to be prefixed with a zero if it is less than 10
  if( $themonth < 10 ) {
    $themonth="0$themonth";
  }

  if( $mday < 10 ) {
    $mday="0$mday";
  }

  if( $hour < 10 ) {
    $hour="0$hour";
  }

  if( $min < 10 ) {
    $min="0$min";
  }

  if( $sec < 10 ) {
    $sec="0$sec";
  }

  $date="$theyear-$themonth-$mday";
  $time="$hour:$min:$sec";
}


# Usage:
# write_logfile($logfile,$msg);
sub write_logfile {
  my $logfile = shift(@_);
  my $msg = shift(@_);
  if (!(-e LOGFILE)) {
    open(LOGFILE, ">>$logfile") or die "Couldn't open $logfile for writing on line: ", __LIN
E__, "\n";
  }
  print LOGFILE "$date:$time $msg\n";
  print "\n$msg\n" if $debug;
  $errors=1;
}



sub my_qx {
  my $command = shift(@_);
  # If the command is inverting the sense of success, i.e. if the command succeededs, 
  # but the program dies and reports an error, pass a second argument to the subroutine
  my $switch = shift(@_);

  print "Command: $command\n" if $debug;

  $err_msg = "Couldn't execute command: '$command'", __FILE__, "on line: ", __LINE__, "$!\n";
  
  if(!($switch)) {
    $result = qx/$command/ || die "$err_msg";
  }
  else {
    $result = qx/$command/ and die "$err_msg";
  }

  print "Result: $result\n" if $debug;

}

sub create_repository {
    my_qx/cvs co $cvs_tag $new_server/ || die "$err_msg"
}

